# cvid_mbiota_oral

This repository contains the R scripts used for the data analysis of our manuscript on oropharyngeal microbiota in Common Variable Immunodeficiency:
https://doi.org/10.3389/fimmu.2020.01245 
Analyses may be applied to different taxonomic levels (only one added here). Sequencing data has been made available on the European Nucleotide Archive (ENA) under project code PRJEB34684.  

Content
*  0: all libraries and functions (always run this script first)
*  1: data prep
*  2: rarefaction curves
*  3: general overview (top 10 most abundant bacteria)
*  4: alpha diversity
*  5: beta diversity
*  6: ANCOM.2
*  6.1: plotting ANCOM results
*  7: bacterial load
*  8: unique taxa (not included in final version of manuscript)
*  9: HRCT scan scores

We are always looking to further develop our data analysis strategies for bacterial community sequencing data. 
Please don't hesitate to contact me if you have any questions or suggestions!