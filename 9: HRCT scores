#9 HRCT scores vs alpha diversity, beta diversity, and BQ

#####################
## alpha diversity ##
#####################
## bacteriadata
otu_mf <- readRDS("merged_L7_8000.RDS")
otu_mf$sampleID<-row.names(otu_mf)

## HRCT data
scores<-read.xlsx("HRCT_scores.xlsx", 1)

## merge them and select only the cvid and XLA patients
merged<-merge(otu_mf, scores, by='sampleID')
cvid<-subset(merged,study_arm == 1|study_arm==2|study_arm==3|study_arm==4|study_arm==5) %>% droplevels() %>%  data.frame
cvid$sampleID<-NULL

## subset the file to ' high'  and ' low' AD (<2 or >10) and ILD scorers (<2 or >10)

otu_AD_high<-subset(cvid, AD.score >10)
otu_AD_low<-subset(cvid, AD.score <2)

otu_ILD_high<-subset(cvid, ILD.score >10)
otu_ILD_low<-subset(cvid, ILD.score <2)

otu_AD_high[,270:ncol(otu_AD_high)] <- NULL
AD_high <- diversity(otu_AD_high, index = "invsimpson") %>% as.data.frame()
AD_high$group<-"AD_high"

otu_AD_low[,270:ncol(otu_AD_low)] <- NULL
AD_low <- diversity(otu_AD_low, index = "invsimpson") %>% as.data.frame()
AD_low$group<-"AD_low"

otu_ILD_high[,270:ncol(otu_ILD_high)] <- NULL
ILD_high <- diversity(otu_ILD_high, index = "invsimpson") %>% as.data.frame()
ILD_high$group<-"ILD_high"

otu_ILD_low[,270:ncol(otu_ILD_low)] <- NULL
ILD_low <- diversity(otu_ILD_low, index = "invsimpson") %>% as.data.frame()
ILD_low$group<-"ILD_low"

all<-as.data.frame(rbind(AD_low, AD_high, ILD_low, ILD_high))
## it needs to be in long format instead of wide

##### if you're unhappy about the order of the bars:
## redefine column 'group' as factor
all$group<-as.factor(all$group)
## check the order of the factors here
print(levels(all$group))
## reorder factors
all$group <- factor(all$group,levels(all$group)[c(2,1,4,3)])

## define colors
my_colors_lung<- RColorBrewer::brewer.pal(11, "Spectral")[c(8,11,5,4)]

#pdf("plots/L7_simpson_imdys_lung_dot.pdf")
#setEPS()
#postscript("plots/L7_simpson_lung_box_dot.eps")
p<-ggplot(all, aes(x=as.factor(group), y=., fill=as.factor(group))) + 
  geom_boxplot(fill="white") +
  geom_dotplot(binaxis='y', stackdir='center', binwidth=0.25)+
  scale_fill_manual(values=my_colors_lung, labels=c("AD <2", "AD >10", "ILD<2", "ILD>10")) + ggtitle("Alpha diversity") +
  theme_classic()
p
#dev.off()

wilcox.test(AD_low$., AD_high$.)
wilcox.test(ILD_low$., ILD_high$.)

####################
## beta diversity ##
####################
## bacteriadata
clr_L5<-readRDS("clr_L5.RDS")
clr_L5$sampleID<-row.names(clr_L5)

## HRCT data
scores<-read.xlsx("HRCT_scores.xlsx", 1)

## metadata
sample<-readRDS("sample_8000.RDS")
sample$sampleID<-row.names(sample)

## merge them and select only the cvid and XLA patients
merged <- merge(clr_L5, sample, by="sampleID")
merged.1<-merge(merged, scores, by='sampleID')
cvid<-subset(merged.1,study_arm == 1|study_arm==2|study_arm==3|study_arm==4|study_arm==5) %>% droplevels() %>%  data.frame
cvid$sampleID<-NULL

cvid$AD_bin[cvid$AD.score <2]<- 'AD < 2'
cvid$AD_bin[cvid$AD.score >10] <- 'AD > 10'
cvid$ILD_bin[cvid$ILD.score <2]<- 'ILD < 2'
cvid$ILD_bin[cvid$ILD.score >10] <- 'ILD > 10'

AD<-subset(cvid, AD_bin=='AD < 2'|AD_bin=='AD > 10')

pca1 <- prcomp(AD[,1:88],
               center = TRUE,
               scale. = TRUE)

summary(pca1)

fit.df <- as.data.frame(pca1$x)
fit.df$group <- AD$AD_bin

my_colors_AD <- RColorBrewer::brewer.pal(11, "Spectral")[c(8,11)]

#setEPS()
#postscript("plots/L5_PCA_AD.eps")
g = ggplot(data=fit.df,aes(x=PC1,y=PC2)) +
  geom_point(aes(colour=fit.df$group)) +
  stat_ellipse(aes(colour=fit.df$group)) + scale_color_manual(values=my_colors_AD)
g= g+ labs(
  x = "PC1 (39%)",
  y = "PC2 (9%)",
  colour = "PCA L5 AD scores"
)
g <- g + theme_classic()
g
#dev.off()

#########
## ILD ##
#########

ILD<-subset(cvid, ILD_bin=='ILD < 2'|ILD_bin=='ILD > 10')

pca1 <- prcomp(ILD[,1:88],
               center = TRUE,
               scale. = TRUE)

summary(pca1)

fit.df <- as.data.frame(pca1$x)
fit.df$group <- ILD$ILD_bin

my_colors_ILD <- RColorBrewer::brewer.pal(11, "Spectral")[c(5,4)]

#setEPS()
#postscript("plots/L5_PCA_ILD.eps")
g = ggplot(data=fit.df,aes(x=PC1,y=PC2)) +
  geom_point(aes(colour=fit.df$group)) +
  stat_ellipse(aes(colour=fit.df$group)) + scale_color_manual(values=my_colors_ILD)
g= g+ labs(
  x = "PC1 (33%)",
  y = "PC2 (8%)",
  colour = "PCA L5 ILD scores"
)
g <- g + theme_classic()
g
#dev.off()

######################
### bacterial load ###
######################

bq_file<-read.table("bq.txt", header=TRUE)

sample<-readRDS("sample.RDS")
sample$sampleID<-row.names(sample)

merged<-merge(bq_file, sample, by="sampleID")
merged.1<-merge(merged, scores, by='sampleID')

cvid<-subset(merged.1,study_arm == 1|study_arm==2|study_arm==3|study_arm==4|study_arm==5) %>% droplevels() %>%  data.frame
cvid$sampleID<-NULL

cvid$AD_bin[cvid$AD.score <2]<- 'AD < 2'
cvid$AD_bin[cvid$AD.score >10] <- 'AD > 10'
cvid$ILD_bin[cvid$ILD.score <2]<- 'ILD < 2'
cvid$ILD_bin[cvid$ILD.score >10] <- 'ILD > 10'

AD <-subset(cvid, AD_bin=='AD < 2'|AD_bin=='AD > 10')
ILD<-subset(cvid, ILD_bin=='ILD < 2'|ILD_bin=='ILD > 10')

## define colors
my_colors_AD <- RColorBrewer::brewer.pal(11, "Spectral")[c(8,11)]
my_colors_ILD <- RColorBrewer::brewer.pal(11, "Spectral")[c(5,4)]

#pdf("plots/bq_AD.pdf")
#setEPS()
#postscript("plots/bq_AD.eps")
AD_bq<-ggplot(AD, aes(x=as.factor(AD_bin), y=log10(BQ), fill=as.factor(AD_bin))) + 
  geom_boxplot(fill="white") +
  geom_dotplot(binaxis='y', stackdir='center', binwidth=0.12)+
  scale_fill_manual(values = my_colors_AD) + ggtitle("Total bacterial load AD") +theme_classic()
AD_bq
#dev.off()

#pdf("plots/bq_ILD.pdf")
#setEPS()
#postscript("plots/bq_ILD.eps")
ILD_bq<-ggplot(ILD, aes(x=as.factor(ILD_bin), y=log10(BQ), fill=as.factor(ILD_bin))) + 
  geom_boxplot(fill="white") +
  geom_dotplot(binaxis='y', stackdir='center', binwidth=0.08)+
  scale_fill_manual(values = my_colors_ILD) + ggtitle("Total bacterial load ILD") +theme_classic()
ILD_bq
#dev.off()

AD_low<-subset(cvid, AD_bin=='AD < 2') %>% droplevels() %>% data.frame
AD_high<-subset(cvid, AD_bin=='AD > 10') %>% droplevels() %>% data.frame
ILD_low<-subset(cvid, ILD_bin=='ILD < 2') %>% droplevels() %>% data.frame
ILD_high<-subset(cvid, ILD_bin=='ILD > 10') %>% droplevels() %>% data.frame

wilcox.test(AD_low$BQ, AD_high$BQ)
wilcox.test(ILD_low$BQ, ILD_high$BQ)

##########################################
## linear regression with bootstrapping ##
##########################################
## bacteriadata
clr_L7<-readRDS("clr_L7.RDS")
clr_L7$sampleID<-row.names(clr_L7)

## HRCT data
scores<-read.xlsx("HRCT_scores.xlsx", 1)

## metadata
sample<-readRDS("sample_8000.RDS")
sample$sampleID<-row.names(sample)

## merge them and select only the cvid and XLA patients
merged <- merge(clr_L6, sample, by="sampleID")
merged.1<-merge(merged, scores, by='sampleID')
cvid<-subset(merged.1,study_arm == 1|study_arm==2|study_arm==3|study_arm==4|study_arm==5) %>% droplevels() %>%  data.frame
cvid$sampleID<-NULL

## define the metadata to be used in the model
cov_L7 <- as.data.frame(cbind(cvid$Age_cont, cvid$gender, cvid$AD.score, cvid$ILD.score, cvid$composite.score, cvid$binary.0.1))
colnames(cov_L7)<-c("Age_cont", "gender", "AD.score", "ILD.score", "composite.score", "binary.0.1")
cov_L7[is.na.data.frame(cov_L7)] <- 0
cov.confounders <- model.matrix(~Age_cont+gender,data=cov_L7)

######
##AD##
######

## y here has to be your outcome, X the rest of the dataset
y <- cov_L7$AD.score
x <- as.matrix(CVID_L7[,1:270])

# Prepare final design matrix
names <- colnames(x)
point_res <- matrix(ncol=4,nrow=ncol(x))

for(i in 1:ncol(x)) {
  fit <- lm(y~cov.confounders[,-1]+x[,i])
  point_res[i,1] <- coef(summary(fit))[4,1]
  point_res[i,2] <- coef(summary(fit))[4,4]
  point_res[i,3] <- summary(fit)$r.squared
}

rownames(point_res) <- names
colnames(point_res) <- c("beta","p.value","R sq", "FDR")

## to allow FDR correction of age and gender as well
point_res<-rbind(point_res,coef(summary(fit))[2,4])
point_res<-rbind(point_res,coef(summary(fit))[3,4])

point_res[,4] <- p.adjust(point_res[,2],method = "BH")

your_data <- data.frame(cbind(y, cov_L6[,c(1,2)], x[,"enter bacterium name here"])) 

bs <- function(d,indices) {
  d <- d[indices,]  
  fit <- lm(y~., data=d)   
  return(coef(fit)) 
}

set.seed(6)
results<- boot(  
  data = your_data, 
  statistic = bs,
  R = 1000                                    
) 

boot.ci(results, type="bca", index=4)
## this index refers to which element of results$t0 it takes; index=2 is age, index=3 is gender, index=4 is the bacterium

######
##ILD##
######

## y here has to be your outcome, X the rest of the dataset
y <- cov_L7$ILD.score
x <- as.matrix(CVID_L7[,1:270])

# Prepare final design matrix
names <- colnames(x)
point_res <- matrix(ncol=4,nrow=ncol(x))

for(i in 1:ncol(x)) {
  fit <- lm(y~cov.confounders[,-1]+x[,i])
  point_res[i,1] <- coef(summary(fit))[4,1]
  point_res[i,2] <- coef(summary(fit))[4,4]
  point_res[i,3] <- summary(fit)$r.squared
}

rownames(point_res) <- names
colnames(point_res) <- c("beta","p.value","R sq", "FDR")


## to allow FDR correction of age and gender as well
point_res<-rbind(point_res,coef(summary(fit))[2,4])
point_res<-rbind(point_res,coef(summary(fit))[3,4])

point_res[,4] <- p.adjust(point_res[,2],method = "BH")

summary(fit)

your_data <- data.frame(cbind(y, cov_L6[,c(1,2)], x[,"enter bacterium name here"])) 

bs <- function(d,indices) {
  d <- d[indices,]  
  fit <- lm(y~., data=d)   
  return(coef(fit)) 
}

set.seed(7)
results<- boot(  
  data = your_data, 
  statistic = bs,
  R = 1000                                    
) 

boot.ci(results, type="bca", index=4)
## this index refers to which element of results$t0 it takes; index=2 is age, index=3 is gender, index=4 is the bacterium

