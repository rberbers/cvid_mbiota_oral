# We take a look at the global community distribution (beta diversity) by plotting a principle component analysis using L5 CLR data. 

L5_CLR$sampleID<-row.names(L5_CLR)
sample$sampleID<-row.names(sample)
merged<- merge(L5_CLR, sample, by="sampleID")

merged$binary.0.1[merged$study_arm=='9']<-'HC'
merged$binary.0.1[merged$study_arm=='5']<-'XLA'

CVID_HC <- subset(merged, imdys=='HC'| imdys=='CVIDio' | imdys=='CVIDid'| imdys=='XLA')
IgA<- subset(CVID_HC, binary.0.1=='HC'| binary.0.1=='0' | binary.0.1=='1'|binary.0.1=='XLA')

pca1 <- prcomp(IgA[,2:89],
               center = TRUE,
               scale. = TRUE)

summary(pca1)

fit.df <- as.data.frame(pca1$x)
fit.df$group <- IgA$binary.0.1

my_colors_iga <- RColorBrewer::brewer.pal(11, "Spectral")[c(3,1,10, 9)]

#pdf("plots/L5_PCA_IgA.pdf")
#setEPS()
#postscript("plots/L5_PCA_IgA_XLA.eps")
g = ggplot(data=fit.df,aes(x=PC1,y=PC2)) +
  geom_point(aes(colour=fit.df$group)) +
  stat_ellipse(aes(colour=fit.df$group)) + scale_color_manual(values=my_colors_iga)
g= g+ labs(
  x = "PC1 (28%)",
  y = "PC2 (8%)",
  colour = "PCA L5"
)
g <- g + theme_classic()
g
#dev.off()

## PERMANOVA
dist_euclid<-vegdist(IgA[,2:89], method="euclid")
set.seed(7)
adonis_IgA<-adonis2(formula=dist_euclid~binary.0.1, data=IgA, permutations=999, method="euclid")
adonis_IgA

pairw.ad<-pairwise.adonis(dist_euclid,IgA$binary.0.1, p.adjust.m = "fdr", sim.method="euclid")
pairw.ad

######################
### no antibiotics ###
######################

merged$antibiotics[merged$imdys=='HC']<-'0'
merged_noab<-subset(merged, antibiotics=='0')


merged_noab$binary.0.1[merged_noab$study_arm=='9']<-'HC'
merged_noab$binary.0.1[merged_noab$study_arm=='5']<-'XLA'

CVID_HC <- subset(merged_noab, imdys=='HC'| imdys=='CVIDio' | imdys=='CVIDid'| imdys=='XLA')
IgA<- subset(CVID_HC, binary.0.1=='HC'| binary.0.1=='0' | binary.0.1=='1'|binary.0.1=='XLA')

pca1 <- prcomp(IgA[,2:89],
               center = TRUE,
               scale. = TRUE)

summary(pca1)

fit.df <- as.data.frame(pca1$x)
fit.df$group <- IgA$binary.0.1

my_colors_iga <- RColorBrewer::brewer.pal(11, "Spectral")[c(3,1,10, 9)]

#pdf("plots/L5_PCA_IgA_XLA_noab.pdf")
#setEPS()
#postscript("plots/L5_PCA_IgA_XLA_noab.eps")
g = ggplot(data=fit.df,aes(x=PC1,y=PC2)) +
  geom_point(aes(colour=fit.df$group)) +
  stat_ellipse(aes(colour=fit.df$group)) + scale_color_manual(values=my_colors_iga)
g= g+ labs(
  x = "PC1 (29%)",
  y = "PC2 (9%)",
  colour = "PCA L5: no antibiotics"
)
g <- g + theme_classic()
g
#dev.off()

## PERMANOVA
dist_euclid<-vegdist(IgA[,2:89], method="euclid")
set.seed(7)
adonis_IgA<-adonis2(formula=dist_euclid~binary.0.1, data=IgA, permutations=999, method="euclid")
adonis_IgA

pairw.ad<-pairwise.adonis(dist_euclid,IgA$binary.0.1, p.adjust.m = "fdr", sim.method="euclid")
pairw.ad